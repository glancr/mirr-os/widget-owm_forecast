# frozen_string_literal: true

require 'test_helper'

class OwmForecast::Test < ActiveSupport::TestCase
  test 'truth' do
    assert_kind_of Module, OwmForecast
  end
end
